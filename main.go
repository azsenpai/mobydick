package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const K = 52 // size of alphabet

type Node struct {
	Parent  *Node
	Edge    byte
	Childes []*Node
	IsLeaf  bool
	Count   int
}

func NewNode(parent *Node, edge byte) *Node {

	return &Node{
		Parent:  parent,
		Edge:    edge,
		Childes: make([]*Node, K),
		IsLeaf:  true,
		Count:   0,
	}
}

func ord(b byte) (byte, bool) {
	if 'a' <= b && b <= 'z' {
		return b - 'a', true
	}

	if 'A' <= b && b <= 'Z' {
		return b - 'A' + K/2, true
	}

	return 0, false
}

func chr(b byte) (byte, bool) {
	if b >= K {
		return 0, false
	}

	if b < K/2 {
		return b + 'a', true
	}

	return b + 'A' - K/2, true
}

func add(node *Node, bytes []byte) {
	if node == nil {
		return
	}

	if len(bytes) == 0 {
		node.Count++
		return
	}

	node.IsLeaf = false
	edge := bytes[0]

	if node.Childes[edge] == nil {
		node.Childes[edge] = NewNode(node, edge)
	}

	add(node.Childes[edge], bytes[1:])
}

func normAdd(node *Node, bytes []byte) {
	normBytes := make([]byte, 0, len(bytes))

	for _, b := range bytes {
		v, ok := ord(b)

		if !ok {
			continue
		}

		normBytes = append(normBytes, v)
	}

	add(node, normBytes)
}

func find(node *Node, count int) *Node {
	if node == nil {
		return nil
	}

	if node.Count == count {
		return node
	}

	if node.IsLeaf {
		return nil
	}

	for edge := 0; edge < K; edge++ {
		if node.Childes[edge] == nil {
			continue
		}

		x := find(node.Childes[edge], count)

		if x != nil {
			return x
		}
	}

	return nil
}

func count(node *Node, pWc *[]int) {
	if node == nil {
		return
	}

	if node.Count > 0 {
		*pWc = append(*pWc, node.Count)
	}

	if node.IsLeaf {
		return
	}

	for edge := 0; edge < K; edge++ {
		if node.Childes[edge] == nil {
			continue
		}

		count(node.Childes[edge], pWc)
	}
}

func CreatePrefixTree() *Node {
	// file, err := os.Open("input.txt")
	file, err := os.Open("mobydick.txt")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	root := NewNode(nil, 0)

	for scanner.Scan() {
		normAdd(root, scanner.Bytes())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return root
}

func qsort(a []int) {
	if len(a) < 2 {
		return
	}

	l := 0
	r := len(a) - 1

	pivot := a[r]
	pivotIndex := 0

	for i := l; i < r; i++ {
		if a[i] <= pivot {
			continue
		}

		a[i], a[pivotIndex] = a[pivotIndex], a[i]
		pivotIndex++
	}

	a[r], a[pivotIndex] = a[pivotIndex], a[r]

	if l < pivotIndex {
		qsort(a[l:pivotIndex])
	}

	if pivotIndex < r {
		qsort(a[pivotIndex+1:])
	}
}

func rev(bytes []byte) {
	n := len(bytes)

	for i := 0; i < n/2; i++ {
		j := n - i - 1
		bytes[i], bytes[j] = bytes[j], bytes[i]
	}
}

func print(bytes []byte) {
	n := len(bytes)

	for i := 0; i < n; i++ {
		fmt.Printf("%c", bytes[i])
	}
}

func main() {
	root := CreatePrefixTree()

	if root == nil {
		return
	}

	wc := []int{}

	count(root, &wc)
	qsort(wc)

	n := 20

	if n > len(wc) {
		n = len(wc)
	}

	for i := 0; i < n; i++ {
		node := find(root, wc[i])

		if node == nil {
			continue
		}

		node.Count = 0
		bytes := []byte{}

		for node.Parent != nil {
			v, ok := chr(node.Edge)
			node = node.Parent

			if !ok {
				continue
			}

			bytes = append(bytes, v)
		}

		rev(bytes)
		print(bytes)

		fmt.Printf(" %d\n", wc[i])
	}
}
